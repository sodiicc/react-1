import React, {useState} from 'react';
import './App.css';
import Counter from './components/Counter'
import {Card} from './components/Card'

function App() {
  const [showCounter, setShowCounter] = useState(false)
  const toggleCounterBtnHendler = () => {
    setShowCounter(!showCounter)
  }
  return (
    <div className="App">
    <button onClick={toggleCounterBtnHendler}>toggle</button>
    {showCounter ? 
    <Counter startValue={0} />
    : null    
    }
    {/* <Counter startValue={10} />
    <Counter startValue={50} hello='hi' /> */}
    <Card />

    </div>
  );
}

export default App;
