import React, { useState, useRef, useEffect } from 'react'
import './style.scss'

export let Card = (props) => {
  const [editStatus, setEditStatus] = useState(false)
  const [title, setTitle] = useState('')
  const [text, setText] = useState('')

  const formElement = useRef()
  const editBtnHandler = ()=> {
    setEditStatus(true)   
  }
  const onSubmitBtnHandler = (e)=> {
    e.preventDefault()
    setTitle(formElement.current[0].value ) 
    setText(formElement.current[1].value)
    setEditStatus(false)
  }

  useEffect(()=>{ 
    if(editStatus){
      // titleInput.current.focus()
    }   
    console.log('update!!!')
  })
  // useEffect(()=>{    
  //   console.log('mounted!!!')
  // }, [])

  return (
    <div>
      {
        !editStatus? 
      <div>
        <h2>{title}</h2>
        <p>{text}</p>
        <button onClick={editBtnHandler}>Edit</button>
      </div>
      :
      <div>
      <form ref={formElement} onSubmit={onSubmitBtnHandler}>
        <input />
        <textarea></textarea>
        <button type="submit">Save</button>

      </form>
      </div>

      }
    </div>
  )
}