import React, { Component } from 'react'
import './style.scss'

export default class Counter extends Component {
  constructor(props) {
    super(props)

    this.state = {
      status: false,
      startValue: this.props.startValue
    }
  }

  increaseCounter = () => {
    this.setState((state) => {
      return { 
        ...state,
        startValue: state.startValue + 1 }
    })
  }

  componentDidMount(){    
    console.log('mounted')
  }
  componentDidUpdate(prevVal, newVal){    
    console.log('updated', prevVal, newVal)
  }
  componentWillUnmount(){    
    console.log('unmount')
  }

  decreaseCounter = () => {
    this.setState((state) => {
      return { 
        ...state,
        startValue: state.startValue - 1 }
    })
  }

  render() {
    return (
      <div className="counter">
        <div className="counterValue">
          {this.state.startValue}
        </div>
        <button className="button" onClick={this.decreaseCounter}>-</button>
        <button className="button" onClick={this.increaseCounter}>+</button>
      </div>
    )
  }
}