import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import './index.css';
import App from './App';
import App2 from './containers/App2';
import {store} from './store/configureStore'
import * as serviceWorker from './serviceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(
  <Provider store={store}>
    <App />
    <App2 />
  </Provider>,
  document.getElementById('root')
)


serviceWorker.unregister();
// registerServiceWorker()
