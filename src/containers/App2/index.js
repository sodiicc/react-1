import React, { Component } from 'react'
import { connect } from 'react-redux'
import { User } from '../../components/User'
import { Page } from '../../components/Page'
import {setYear} from '../../actions/pageActions'

import './style.css'

class App2 extends Component {
  render() {
    const { user, page, setYearAction } = this.props
    console.log('this.props', this.props)
    return (
      <div className="App2">
        <header className="App2-header">
          <h1 className="App2-title">Мой топ фото</h1>
        </header>
        <User name={user.name} />
        <Page photos={page.photos} year={page.year} setYear={setYearAction} />
      </div>
    )
  }
}

const mapStateToProps = store => {
  console.log('store', store)
  return {
    user: store.user,
    page: store.page
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setYearAction: year => dispatch(setYear(year))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App2)