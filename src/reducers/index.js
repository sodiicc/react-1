import {combineReducers} from 'redux'
import {pageReducer} from './page'
import {userReducer} from './user'

export const initialState = {
  user: {
    name: 'Vasiliy',
    surname: 'Tsiva',
    age: 34
  }
}

export const rootReducer = combineReducers({
  page: pageReducer,
  user: userReducer
})